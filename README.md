# **Introduction**
------
This script provided the function that can automatically connect to internet via static IP on boot and set the virtual interfaces.

# **Usage**
------
Run the interactive installation script as follows.
```
$ ./install
```
When installation completes, you should check the ifconfig and /etc/resolv.conf to verify the
IP and DNS. Then, ping the google DNS and NTU web server to verify the
connectivity and DNS service.
```
$ ping 8.8.8.8
$ ping www.ntu.edu.tw
```
The above ping should work and return some information.
Finally, chech the outgoing IP address.
```
$ curl ifconfig.me
```

# **Author**

Jason Chen
